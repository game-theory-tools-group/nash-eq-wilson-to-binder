Welcome to the repository hosting 3 ways to use Jupyter instance hosting notebooks on the GTnash library.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fforgemia.inra.fr%2Fgame-theory-tools-group%2Fnash-eq-wilson-to-binder/HEAD)

# Nash Eq Wilson To Binder (aka the Binder way)

This repository is hosting a Dockerfile enabling binder to expose the Nash Eq Wilson Project Notebooks.

More information on [Binder](https://mybinder.readthedocs.io/en/latest/#)

By clicking on the icon on top, you should acces to a jupyter instance hosting the notebooks.

# Also available on SIWAA (aka the SIWAA way)

[SIWAA](https://siwaa.toulouse.inrae.fr/) is a web site hosting interactive tools and applications.

A direct access to the jupyter launcher --> https://siwaa.toulouse.inrae.fr/root?tool_id=interactive_nash_eq_wilson 

# To launch a Jupyter notebook hosting the Project on your own laptop (aka the personnal way)

This is another way to use the notebooks by launching a jupyter instance on your localhost.

Copy/Paste the command line on a teminal.
```
docker run -p 8888:8888 registry.forgemia.inra.fr/game-theory-tools-group/nash-eq-wilson-to-binder/nash-eq-wilson
```
and click this link http://0.0.0.0:8888/

# (in case) To manage the container

* `docker container list` : to list the containers running and get the ids
* `docker stop [idOfTheContainer]` : to stop the container
* `docker pull registry.forgemia.inra.fr/game-theory-tools-group/nash-eq-wilson-to-binder/nash-eq-wilson` : to update the local image of the container.

# (in case) To install docker

A straightforward and simple tuto dedicated to the 20.04 of Ubuntu : [Comment installer Docker sur Ubuntu 20.04 LTS Focal Fossa
](https://linuxconfig-org.translate.goog/how-to-install-docker-on-ubuntu-20-04-lts-focal-fossa?_x_tr_sl=en&_x_tr_tl=fr&_x_tr_hl=fr&_x_tr_pto=nui,sc)

# To update the image of the container hosted on the [registry](https://forgemia.inra.fr/game-theory-tools-group/nash-eq-wilson-to-binder/container_registry) of this repository

Follow the link:

https://forgemia.inra.fr/game-theory-tools-group/nash-eq-wilson-to-binder/-/pipelines/new


